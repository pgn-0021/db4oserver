package br.edu.estacio;
import com.db4o.ObjectServer;
import com.db4o.cs.Db4oClientServer;

public class Server {

	public static void main(String[] args) {
		System.out.println("Database for Objects is running on port 3381");
		System.out.println("Hit Ctrl-C to exit");
		new Server().runServer();
	}	
		
	public void runServer() {
		synchronized (this) {
			ObjectServer s = Db4oClientServer.openServer(Db4oClientServer.newServerConfiguration(),"/home/dell/dataServer.dbo", 3381);
			s.grantAccess("db4o","db4o");
			Thread.currentThread().setName(this.getClass().getName());
			Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
			try { 
				this.wait(Long.MAX_VALUE);
			} catch (Exception e) {
				e.printStackTrace();
			}
			s.close();
		}
	}
	
}
